import json
import pyjokes

def lambda_handler(event, context):
    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": pyjokes.get_joke()
        }),
    }
